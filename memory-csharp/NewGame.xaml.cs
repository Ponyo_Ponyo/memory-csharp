﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace memory_csharp
{
    /// <summary>
    /// Interaction logic for NewGame.xaml
    /// </summary>
    public partial class NewGame : Page
    {
        

        public NewGame()
        {
            InitializeComponent();
        }
        private void Button_Back(object sender, RoutedEventArgs e)
        {
            
            this.NavigationService.Navigate(new Titlescreen());
        }
        /// <summary>
        /// Bij het drukken van de Quit button word je uit het spel gegooit.
        /// </summary>
        /// <param name="sender">  sender is de button die gedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Start(object sender, RoutedEventArgs e)
        {
            if (Name2.Text.Length > 0)
            {
                GlobalVariable.player2 = Name2.Text;
                GlobalVariable.playertwo = true;
              
            }
            else
            {
                GlobalVariable.playertwo = false;
            }
            GlobalVariable.pictureTypeStandard = (bool)Standard.IsChecked;
            if (Name1.Text.Length > 0)
            {
                GlobalVariable.player1 = Name1.Text;
            }

            this.NavigationService.Navigate(new Game());
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
