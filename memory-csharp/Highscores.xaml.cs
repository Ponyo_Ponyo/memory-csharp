﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace memory_csharp
{
    /// <summary>
    /// Interaction logic for Highscores.xaml
    /// </summary>
    public partial class Highscores : Page
    {
      
        public Highscores()
        {
            InitializeComponent();
            if (File.Exists("highscores.sav"))
            {
                List<string> Scores = System.IO.File.ReadLines("highscores.sav").ToList();
                
                if (Scores.Count > 0)
                {
                    //Scheidt de scores en namen van elkaar en zet ze gesorteerd in een lijst met arrays.
                    List<string[]> FormattedScores = new List<string[]>();
                    int count = Scores.Count;
                    for (int i = 0; i < count; i++)
                        FormattedScores.Add(Scores[i].Split(':'));

                    //zet de scores en hun namen in de juiste labels
                    S1.Content = FormattedScores[0][0];
                    Naam1.Content = FormattedScores[0][1];
                    if (FormattedScores.Count == 1) goto brake;
                    
                    S2.Content = FormattedScores[1][0];
                    Naam2.Content = FormattedScores[1][1];
                    if (FormattedScores.Count == 2) goto brake;
                    
                    S3.Content = FormattedScores[2][0];
                    Naam3.Content = FormattedScores[2][1];
                    if (FormattedScores.Count == 3) goto brake;
                    
                    S4.Content = FormattedScores[3][0];
                    Naam4.Content = FormattedScores[3][1];
                    if (FormattedScores.Count == 4) goto brake;
                    
                    S5.Content = FormattedScores[4][0];
                    Naam5.Content = FormattedScores[4][1];
                brake:;
                }
            }
        }

        /// <summary>
        /// Bij het drukken van de Titlescreen button word je naar het Startscherm gebracht.
        /// </summary>
        ///  /// <param name="sender">  sender is de button die gedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Titlescreen(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Titlescreen());
        }
    }
}
