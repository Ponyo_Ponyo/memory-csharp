﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace memory_csharp
{
    public class GlobalVariable
    {
        public static int [] picture = new int[Game.NR_OF_COLLUMNS*Game.NR_OF_ROWS];

        public static bool [] isEnabledButton = new bool[Game.NR_OF_COLLUMNS * Game.NR_OF_ROWS];

        public static int currentPlayer;

        public static string player1 = "Player1";
        public static string player2;

        public static bool playertwo;

        public static int score1 = 0;
        public static int score2 = 0;

        public static int winnerglobal= 0;
        public static string tag = "-1";
        
        public static bool pictureTypeStandard;

        public GlobalVariable()
        {
            initialize();
        }
        /// <summary>
        /// 
        /// </summary>
        public static void initialize()
        {
            for (int i = 0; i < (Game.NR_OF_COLLUMNS * Game.NR_OF_ROWS); i++)
            {
                isEnabledButton[i] = true;
            }
            currentPlayer = 0;

            player1 = "Player1";
            player2 = null;

            playertwo=false;

            score1 = 0;
            score2 = 0;
        }
    }

}

      

