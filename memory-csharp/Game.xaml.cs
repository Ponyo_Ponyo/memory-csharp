﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Threading;

namespace memory_csharp
{
    
    public partial class Game : Page
    {
        Button buttonClick1 = null;
        Button buttonClick2 = null;
        const int waitingTime = 1000; // millisecs
        bool Enable = true;
        bool Disable = false;
        bool twoPlayer = false;
        
        int winner = GlobalVariable.winnerglobal; //houdt bij of je hebt gewonnen

        public const int NR_OF_ROWS = 4;
        public const int NR_OF_COLLUMNS = 4;
       
        Button [,] buttonArray = new Button[NR_OF_ROWS, NR_OF_COLLUMNS];

        /// <summary>
        /// Deze enum geeft de waarde aan van de actieve player voor het opslaan.
        /// </summary>
   
        enum ActivePlayer
        {
            Player1 = 0,
            Player2 = 1
        }
        ActivePlayer CurrentPlayer = ActivePlayer.Player1;

       
        public Game()
        {
            InitializeComponent();
          
            CreateBoard();

            Player1name.Content = GlobalVariable.player1;
            Player2name.Content = GlobalVariable.player2;
            CurrentPlayer = (ActivePlayer)GlobalVariable.currentPlayer;
            setActivePlayer(CurrentPlayer);

            Score1.Content = GlobalVariable.score1;
            if (GlobalVariable.player2 != null)
            {
                twoPlayer = true;
                Score2.Content = GlobalVariable.score2;

            }
            if (GlobalVariable.tag.Equals("-1"))
            {
               buttonClick1 = null;
            }
            else
            {
                foreach ( Button b in buttonArray)
                {
                    if (b.Tag.Equals(GlobalVariable.tag))
                    {
                        buttonClick1 = b;
                    }
                }
            }

            int index = 0;
            for( int i =0; i < Game.NR_OF_ROWS; i++ )
            {
                for( int j=0; j < Game.NR_OF_COLLUMNS; j++ )
                {
                    buttonArray[i, j].IsEnabled = GlobalVariable.isEnabledButton[index];
                    index++;
                    if(buttonArray[i, j].IsEnabled == false)
                    {
                        showPicture(buttonArray[i, j]);
                    }
                }
            }
        }

        int one     = GlobalVariable.picture[0];  //Maakt variablen gelijk aan de 
        int two     = GlobalVariable.picture[1];  //GlobalVariable, de variabelen in GlobalVariable
        int three   = GlobalVariable.picture[2];  //worden gezet wanneer je op "New"
        int four    = GlobalVariable.picture[3]; //en "Reset" drukt.
        int five    = GlobalVariable.picture[4]; //
        int six     = GlobalVariable.picture[5];  //
        int seven   = GlobalVariable.picture[6];  //
        int eight   = GlobalVariable.picture[7]; //
        int nine    = GlobalVariable.picture[8];  //
        int ten     = GlobalVariable.picture[9];  //
        int eleven  = GlobalVariable.picture[10]; //
        int twelf   = GlobalVariable.picture[11]; //
        int thirteen = GlobalVariable.picture[12]; //
        int fourteen = GlobalVariable.picture[13]; //
        int fifteen = GlobalVariable.picture[14]; //
        int sixteen = GlobalVariable.picture[15];//

        /// <summary>
        /// Deze functie brushed de buttons naar de desbetreffende plaatje die uit de randomizer komt
        /// </summary>
        /// <param name="button"> dat is de knop die je indrukt.</param>
        private void showPicture(Button button )
        {
            var brush = new ImageBrush();
            if (GlobalVariable.pictureTypeStandard == true)
            {
                brush.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/assets/cards/" + GlobalVariable.picture[Int32.Parse((String)button.Tag)] + ".jpg"));
            }
            else
            {
                brush.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/assets/cards/superhero/" + GlobalVariable.picture[Int32.Parse((String)button.Tag)] + ".jpg"));
            }
            button.Background = brush;
        }

        /// <summary>
        ///  Hier word het speelveld gecreeerd door kolommen en rijen toe te voegen en daar buttons toe te voegen met een eigen tag.
        ///  Met die tag kan je de buttons van elkaar te onderscheiden.
        /// </summary>
        private void CreateBoard()
        {
            post.HorizontalAlignment = HorizontalAlignment.Left;

            for (int i = 0; i < NR_OF_ROWS; i++)
            {
                RowDefinition r = new RowDefinition();
                r.Height = new GridLength(170);
                post.RowDefinitions.Add(r);
            }
            for (int i = 0; i < NR_OF_COLLUMNS; i++)
            {
                ColumnDefinition c1 = new ColumnDefinition();
                c1.Width = new GridLength(125);
                post.ColumnDefinitions.Add(c1);
            }

            int count = 0;
            for (int row = 0; row < NR_OF_ROWS; row++)
            {
                for (int column = 0; column < NR_OF_COLLUMNS; column++)
                {
                    
                    Button button = new Button();
                    buttonArray[row,column] = button;
                    button.Click += ButtonClick;
                    button.Height = 163;
                    button.Width = 118;
                    button.Style = (Style)FindResource("cards");
                    button.Tag = Convert.ToString(count++);
                    post.Children.Add(button);
                    Grid.SetRow(button, row);
                    Grid.SetColumn(button, column);
                   
                }
            }

        }
        /// <summary>
        /// Je wordt verwezen naar een functie die de plaatjes laat zien en de click veroorzaakt.
        /// </summary>
        /// <param name="sender"> sender is de button die ingedrukt word.</param>
        /// <param name="e"></param>
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (buttonClick2 != null)
            {
                return;
            }
            Button button = (Button)sender;
            showPicture(button);
            handleClick((Button)sender);
        }

        /// <summary>
        /// Als een button clickt dan kun je hem niet nogmaals indrukken en als je 2 dezelfde plaatjes hebt dan blijven deze omgedraait.
        /// Als het een match is worden er 100 punten bij je score toegevoegt en als je het fout hebt gaan er 10 van af.
        /// Bij 8 matches word de speler met de hoogste score als winnaar toegekent.
        /// </summary>
        /// <param name="buttonClick"> Dit gebeurd bij het clicken van een button.</param>
        private async void handleClick(Button buttonClick)
        {
            if (buttonClick1 == null)
            {
                buttonClick1 = buttonClick;
                buttonClick1.IsEnabled = Disable;
            }
            else if (buttonClick2 == null)
            {
                buttonClick2 = buttonClick;

                ImageBrush test1 = (ImageBrush)buttonClick1.Background;
                ImageBrush test2 = (ImageBrush)buttonClick2.Background;

                if (test1.ImageSource.ToString().Equals(test2.ImageSource.ToString()))
                {
                   

                    if (CurrentPlayer == ActivePlayer.Player1)
                    {
                        GlobalVariable.score1 = GlobalVariable.score1 + 100;
                        Score1.Content = GlobalVariable.score1;
                    }
                    else
                    {
                        GlobalVariable.score2 = GlobalVariable.score2 + 100;
                        Score2.Content = GlobalVariable.score2;
                    }

                    winner++;
                    if (winner == 8)
                    {
                        if (GlobalVariable.score1 > GlobalVariable.score2)
                        {
                            gewonnen.Content = Player1name.Content + " WON!!!";
                        }
                        if (GlobalVariable.score2 > GlobalVariable.score1)
                        {
                            gewonnen.Content = Player2name.Content + " WON!!!";
                        }
                        if (GlobalVariable.score1 == GlobalVariable.score2 && GlobalVariable.playertwo == true)
                        {
                            gewonnen.Content = "TIE";
                        }
                        else if (GlobalVariable.score1 == GlobalVariable.score2)
                        {
                            gewonnen.Content = Player1name.Content + " WON!!!";
                        }

                        if (GlobalVariable.playertwo == false) { HighScores(); 
                        }
                    }
                    buttonClick2.IsEnabled = false;
                }
                else
                {
                    await Task.Delay(waitingTime);
                    var logo = new ImageBrush();
                    logo.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/assets/cards/back1.png"));
                    buttonClick1.Background = logo;
                    buttonClick2.Background = logo;
                    buttonClick1.IsEnabled = Enable;
                    buttonClick2.IsEnabled = Enable;

                    if (CurrentPlayer == ActivePlayer.Player1)
                    {
                        GlobalVariable.score1 = GlobalVariable.score1 - 10;
                        Score1.Content = GlobalVariable.score1;

                    }
                    else
                    {
                        GlobalVariable.score2 = GlobalVariable.score2 - 10;
                        Score2.Content = GlobalVariable.score2;

                    }
                    SwitchTurn();
                }
                buttonClick1 = null;
                buttonClick2 = null;
            }
            return;
        }
        /// <summary>
        /// Deze functie gaat kijken wie de actieve speler is en schakelt elke keer als een speler fout gokt.
        /// </summary>
        private void SwitchTurn()
        {
            if (CurrentPlayer == ActivePlayer.Player1)
            {
                if (twoPlayer)
                {
                    setActivePlayer(ActivePlayer.Player2);
                }
            }
            else
            {
                setActivePlayer(ActivePlayer.Player1);

            }
        }
        /// <summary>
        /// Deze functie gaat kijken wie de actieve speler is en plaatst een > voor zijn naam en schakelt als een persoon fout gokt.
        /// </summary>
        /// <param name="player"> Dit refereert naar de speler die aan de beurt is.</param>
        private void setActivePlayer(ActivePlayer player)
        {
            if (player == ActivePlayer.Player1)
            {
                if (twoPlayer)
                {
                    CurrentPlayer = ActivePlayer.Player1;
                    turn2.Content = "";
                    turn1.Content = ">";
                }
            }
            else
            {
                CurrentPlayer = ActivePlayer.Player2;
                turn1.Content = "";
                turn2.Content = ">";
            }
        }

        /// <summary>
        /// Wanneer je singleplayer speelt en je hebt gewonnen zullen de score en naam in het highscores.sav bestand worden
        /// opgeslagen als de score hoger is dan die al in het highscores.sav bestand aanwezig zijn.
        /// Is er nog geen highscores.sav bestand, dan zal deze nu worden aangemaakt.
        /// </summary>
        private void HighScores()
        {
            if (!File.Exists("highscores.sav"))
            {
                string Score = GlobalVariable.score1 + ":" + GlobalVariable.player1;
                using (StreamWriter writeScore = new StreamWriter("highscores.sav"))
                    writeScore.WriteLine(Score);
            }
            else
            {
                List<string> Scores = System.IO.File.ReadLines("highscores.sav").ToList();
                string NewScore = GlobalVariable.score1 + ":" + GlobalVariable.player1;

                Scores.Add(NewScore);

                var SortedScores = Scores
                    .Select(p => new { num = int.Parse(p.Split(':')[0]), name = p.Split(':')[1] })
                    .OrderBy(p => p.num)
                    .ThenBy(p => p.name)
                    .Select(p => string.Format("{0}:{1}", p.num, p.name))
                    .ToList();
                SortedScores.Reverse();

                if (SortedScores.Count <= 5)
                {
                    using (StreamWriter writeScores = new StreamWriter("highscores.sav"))
                        foreach (string s in SortedScores) { writeScores.WriteLine(s); }
                }
                else
                {
                    using (StreamWriter writeScores = new StreamWriter("highscores.sav"))
                        for (int i = 0; i < 5; i++) { writeScores.WriteLine(SortedScores[i]); }
                }
            }
        }

        /// <summary>
        /// Bij het drukken van de Quit button wordt je uit het spel gegooit.
        /// </summary>
        /// <param name="sender"> sender is de button die ingedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Quit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        /// <summary>
        /// Bij het indrukken van de titlescreen button word je naar het hoofdscherm gestuurd.
        /// </summary>
        /// <param name="sender">sender is de button die ingedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Titlescreen(object sender, RoutedEventArgs e)
        {
            GlobalVariable.score1 = 0;
            GlobalVariable.score2 = 0;
            this.NavigationService.Navigate(new Titlescreen());
        }

        /// <summary>
        /// Bij het indrukken van de Reset button worden de kaarten gehusseld en wordt de score op 0 gezet, 
        /// de namen blijven hetzelfde en alle kaarten worden terug omgedraait.
        /// </summary>
        /// <param name="sender"> sender is de button die ingedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Reset(object sender, RoutedEventArgs e)
        {

            int[] myNum = { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8 };   //Fisher-Yates shuffle wordt gebruikt
                                                                                //om array te husselen.
            Random random = new Random();                                       //
                                                                                //
            for (int i = myNum.Length - 1; i > 0; i--)                          //
            {                                                                   //
                int randomIndex = random.Next(0, i + 1);                        //
                                                                                //
                int temp = myNum[i];                                            //
                myNum[i] = myNum[randomIndex];                                  //
                myNum[randomIndex] = temp;                                      //
            }                                                                   //

            GlobalVariable.picture = myNum;

            GlobalVariable.score1 = 0;
            GlobalVariable.score2 = 0;

            for (int i = 0; i < (Game.NR_OF_COLLUMNS * Game.NR_OF_ROWS); i++)
            {
                GlobalVariable.isEnabledButton[i] = true;
            }

            GlobalVariable.currentPlayer = 0;

            this.NavigationService.Navigate(new Game());
        }

        /// <summary>
        /// Bij het drukken van de save button wordt de staat van de kaarten opgeslagen, de scores, 
        /// de namen en wie er aan de beurt is.
        /// </summary>
        /// <param name="sender"> sender is de button die ingedrukt word.</param>
        /// <param name="e"></param>
        private async void Button_Save(object sender, RoutedEventArgs e)
        {
            await Task.Delay(waitingTime);
            int score1local = GlobalVariable.score1;
            int score2local = GlobalVariable.score2;
            int number0 = GlobalVariable.picture[0];
            int number1 = GlobalVariable.picture[1];
            int number2 = GlobalVariable.picture[2];
            int number3 = GlobalVariable.picture[3];
            int number4 = GlobalVariable.picture[4];
            int number5 = GlobalVariable.picture[5];
            int number6 = GlobalVariable.picture[6];
            int number7 = GlobalVariable.picture[7];
            int number8 = GlobalVariable.picture[8];
            int number9 = GlobalVariable.picture[9];
            int number10 = GlobalVariable.picture[10];
            int number11 = GlobalVariable.picture[11];
            int number12 = GlobalVariable.picture[12];
            int number13 = GlobalVariable.picture[13];
            int number14 = GlobalVariable.picture[14];
            int number15 = GlobalVariable.picture[15];

            GlobalVariable.currentPlayer = (int)CurrentPlayer;

            using (StreamWriter writetext = new StreamWriter("memory.sav"))
            {
                writetext.WriteLine(GlobalVariable.player1);        //Player 1 Naam
                writetext.WriteLine(score1local);                   //Player 1 Score
                writetext.WriteLine(GlobalVariable.player2);        //Player 2 Naam
                writetext.WriteLine(score2local);                   //Player 2 Score
                writetext.WriteLine("Plaatsen:");
                writetext.WriteLine(number0);
                writetext.WriteLine(number1);
                writetext.WriteLine(number2);
                writetext.WriteLine(number3);
                writetext.WriteLine(number4);
                writetext.WriteLine(number5);
                writetext.WriteLine(number6);
                writetext.WriteLine(number7);
                writetext.WriteLine(number8);
                writetext.WriteLine(number9);
                writetext.WriteLine(number10);
                writetext.WriteLine(number11);
                writetext.WriteLine(number12);
                writetext.WriteLine(number13);
                writetext.WriteLine(number14);
                writetext.WriteLine(number15);
  
               

                for (int row = 0; row < NR_OF_ROWS; row++)
                {
                    for( int col=0; col < NR_OF_COLLUMNS; col++)
                    {
                        writetext.WriteLine(buttonArray[row, col].IsEnabled);
                    }
                }

                writetext.WriteLine((int)CurrentPlayer);
                writetext.WriteLine(winner);
                writetext.WriteLine(GlobalVariable.pictureTypeStandard);
                if (buttonClick1 == null)
                {
                    writetext.WriteLine("-1"); // Geen buttons ingedrukt.
                }
                else
                {
                    writetext.WriteLine(buttonClick1.Tag);
                }
            }
            MessageBox.Show("Game is Saved", "MemoryGame");
        }


    }
    
}