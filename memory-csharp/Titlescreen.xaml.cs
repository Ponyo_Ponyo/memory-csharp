﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace memory_csharp
{
    
    /// <summary>
    /// Interaction logic for Titlescreen.xaml
    /// </summary>
    public partial class Titlescreen : Page
    {

        static SoundPlayer muziekPlayer = null;

        public Titlescreen()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Deze functie start de muziek en zorgt er ook voor dat deze oneindig lang zal doorloopen.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"> </param>
        public void Button_Music_on(object sender, RoutedEventArgs e)
        {

            if (muziekPlayer == null)
            {
                muziekPlayer = new SoundPlayer(Properties.Resources.Music);    //Speelt de muziek die in Resource.resx is gezet
                muziekPlayer.PlayLooping();                                             //Herhaald de muziek opnieuw
            }
        }


        /// <summary>
        /// Deze functie stopt de muziek.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"> </param>
        private void Button_Music_off(object sender, RoutedEventArgs e)
        {
            if (muziekPlayer != null)
            {
                muziekPlayer.Stop();
                muziekPlayer = null;
            }

        }
        /// <summary>
        /// Deze functie word gebruikt bij het starten van een nieuwe game.
        /// Hij gaat door een randomizer heen en je kan namen invullen.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"> </param>
        private void Button_New(object sender, RoutedEventArgs e)
        {
            int[] myNum = { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8 };   //Fisher-Yates shuffle wordt gebruikt
                                                                                //om array te randomizen
            Random random = new Random();                                       //
                                                                                //
            for (int i = myNum.Length - 1; i > 0; i--)                          //
            {                                                                   //
                int randomIndex = random.Next(0, i + 1);                        //
                                                                                //
                int temp = myNum[i];                                            //
                myNum[i] = myNum[randomIndex];                                  //
                myNum[randomIndex] = temp;                                      //
            }                                                                   //
            GlobalVariable.initialize();
            GlobalVariable.picture = myNum;
            GlobalVariable.score2 = 0;
            this.NavigationService.Navigate(new NewGame());
        }

        /// <summary>
        /// Deze functie word gebruikt bij het loaden van een save game.
        /// De Namen, scores en de status van de kaarten worden geladen.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Load(object sender, RoutedEventArgs e)
        {
            GlobalVariable.playertwo = false;
            GlobalVariable.player2 = null;

            if (File.Exists("memory.sav"))
            {
                using (StreamReader readtext = new StreamReader("memory.sav"))
                {

                    GlobalVariable.player1 = readtext.ReadLine();                             //Player 1 Naam
                    GlobalVariable.score1 = Int32.Parse(readtext.ReadLine());                 //Player 1 Score
                    GlobalVariable.player2 = readtext.ReadLine();                             //Player 2 Naam
                    if (GlobalVariable.player2.Length == 0)
                    {
                        GlobalVariable.player2 = null;
                        GlobalVariable.playertwo = false;
                    }
                    GlobalVariable.score2 = Int32.Parse(readtext.ReadLine());                 //Player 2 Score
                    readtext.ReadLine();
                    for (int i = 0; i < (Game.NR_OF_COLLUMNS * Game.NR_OF_ROWS); i++)
                    {
                        GlobalVariable.picture[i] = Int32.Parse(readtext.ReadLine());
                    }
                    for (int i = 0; i < (Game.NR_OF_COLLUMNS * Game.NR_OF_ROWS); i++)
                    {
                        GlobalVariable.isEnabledButton[i] = Convert.ToBoolean(readtext.ReadLine());
                    }
                    
                    
                   
                    
                    GlobalVariable.currentPlayer = Int32.Parse(readtext.ReadLine());
                    GlobalVariable.winnerglobal = Int32.Parse(readtext.ReadLine());
                    GlobalVariable.pictureTypeStandard = Convert.ToBoolean(readtext.ReadLine());
                    GlobalVariable.tag = readtext.ReadLine();
                    this.NavigationService.Navigate(new Game());

                }
            }

            else
                {
                    MessageBox.Show("memory.sav not found", "Error");
                }
            
           
        }
        /// <summary>
        /// Bij het drukken van de Highscore button kom je in een highscore scherm en zie je de hoogste score.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Highscores(object sender, RoutedEventArgs e)
        {
            
            this.NavigationService.Navigate(new Highscores());
        }
        /// <summary>
        /// Bij het drukken van de Quit button word je uit het spel gegooit.
        /// </summary>
        /// <param name="sender"> sender is de button die gedrukt word.</param>
        /// <param name="e"></param>
        private void Button_Quit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

       
    }
}
